<script type="text/javascript">
    jQuery(document).ready(function() {
        oTable = $('#tbl1').dataTable({
            "sPaginationType": "full_numbers",
            "bProcessing": false,
            "responsive": true,
            "sAjaxSource": "http:\/\/localhost\/arm\/api\/urls",
            //"sAjaxSource": "http:\/\/arm.ceniack.com\/api\/urls",
            "bServerSide": true,
            "aoColumnDefs": [
                {
                    "aTargets": ['_all'],
                    "mRender": function(data, type, full) {
                        if (data != null)
                        {
                            return '<div class="test" style="/*text-overflow:ellipsis;*/">' + data + '</div>';
                        }
                        else
                        {
                            return '';
                        }
                    }
                }
            ],
            "columns": [
                {className: "hide-for-small"}, //1st column
                {className: "hide-for-small"}, //2nd column
                {className: ""}, //3rd column
                {className: "hide-for-small"}, //4th column
                {className: "hide-for-small"}, //5th column
                {className: "hide-for-small"}, //6th column
                {className: ""}, //7th column
                {className: ""}, //8th column
                {className: "show-for-large-up"}, //9th column
                {className: ""}, //10th column
                {className: "hide-for-small"}, //11th column
                {className: "hide-for-small"}, //12th column
                {className: "hide-for-small"}, //13th column
                {className: "hide-for-small"}, //14th column
                {className: "hide-for-small"}, //15th column
            ]
        });
    });
</script>
