<?php

/*
 * class to run cron job.
 */

class CronJobController extends \BaseController {

    /**
     * cron job method to get analytics of each url and store into url_analytics table.
     * Runs Daily.
     */
    public function cronJob() {

        $record = new UrlAnalytics();
        $record->url_data_id = 3;
        $record->daily_total = 10;
        $record->raw_data = 'test data to check cron job runs';
        $record->save();
        exit();
        $urls = UrlData::all(array('id', 'short_url'))->toArray();

        //create google client to get short analytics
        $client = Url::getClient($this->client_id, $this->client_secret, $this->redirect_uri);
        $client->setDeveloperKey($this->developerKey);
        $record = null;
        foreach ($urls as $item) {

            $record = new UrlAnalytics();
            $record->url_data_id = $item['id'];
            //get short url analytics as an object
            $analytics = Url::getShortAnalytics($client, $item['short_url']);
            //set daily total field click.
            $record->daily_total = $analytics->modelData['analytics']['day']['shortUrlClicks'];
            // convert return object into json format.
            $analytics = json_encode($analytics);
            //$analytics=json_decode($analytics, true);
            $record->raw_data = $analytics;
            $record->save();
        }
        return 'helo';
    }

}
