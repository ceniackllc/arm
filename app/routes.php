<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the Closure to execute when that URI is requested.
  |
 */
Route::group(array('before' => 'auth.custom'), function() {

    //route for index page, call index method of controller
    Route::get('/dashboard', 'UrlsController@index');


    Route::model('url', 'Url');
    // route for long to short URL conversion.
    Route::get('/longToShort', 'UrlsController@longToShort');

    // route for url builder page.
    Route::get('/urlBuilder', 'UrlsController@urlBuilder');

    Route::get('/delete/{url}', 'UrlsController@delete');

    Route::post('/delete', 'UrlsController@handleDelete');

    // route for form submission call handle long to short method.
    Route::post('/longToShort', 'UrlsController@handleLongToShort');

    // route for handle urlBuilder form submission.
    Route::post('/urlBuilder', 'UrlsController@handleUrlBuilder');
    // route for save call to save method.
    Route::post('/save', 'UrlsController@save');
    // route for save build url data int url_data table.
    Route::post('/save_url', 'UrlsController@saveBuildUrl');

    // route for   short to long URL conversion.
    Route::get('/shortToLong', 'UrlsController@shortToLong');

    // route for form submission call handle   short to long method.
    Route::post('/shortToLong', 'UrlsController@handleShortToLong');
    // route for   showing short url form, then get its analytics.
    Route::get('/shortAnalytics', 'UrlsController@shortAnalytics');

    // route for handle shortAnalytics request.
    Route::post('/shortAnalytics', 'UrlsController@handleShortAnalytics');
    // route csv export
    Route::post('/export', 'UrlsController@exportCsv');
    //routes needed for datatable
    Route::resource('urls', 'UrlsController');
    Route::get('api/urls', array('as' => 'api.urls', 'uses' => 'UrlsController@getDatatable'));

    // test route to just run code snippet 
    Route::get('/test', function() {

        $data = UrlData::find(6)->analytics()->whereBetween('pulled_at', array('2014-08-21 15:39:08', '2014-08-27 15:39:08'))->get()->toArray();
        echo "<pre>";
        echo "Record obtained between two dates";
        var_dump($data);
        echo "first  Analytics array with old date.<br>";
        $analytics = $data[0]['raw_data'];
        $analytics = json_decode($analytics, true);
        print_r($analytics);
        echo "second  Analytics array with most recent date <br>";
        $analytics1 = $data[2]['raw_data'];
        $analytics1 = json_decode($analytics1, true);
        echo "<br> analytics1<br>";
        print_r($analytics1);

        $calculated_analytics = array();
        $calculated_analytics['shortUrlClicks'] = ($analytics1['modelData']['analytics']['allTime']['shortUrlClicks']) - ($analytics['modelData']['analytics']['allTime']['shortUrlClicks']);
        $calculated_analytics['longUrlClicks'] = ($analytics1['modelData']['analytics']['allTime']['longUrlClicks']) - ($analytics['modelData']['analytics']['allTime']['longUrlClicks']);
        if (array_key_exists('referrers', $analytics['modelData']['analytics']['allTime'])) {
            //$calculated_analytics['referrers']=($analytics1['modelData']['analytics']['allTime']['referrers'])-($analytics['modelData']['analytics']['allTime']['referrers']);
        }
        echo "calculated analytics<br><br>";
        print_r($calculated_analytics);
        exit();
        $user = Confide::user();
        echo "user id = " . $user->id . "<br>";
        $roles = array();

        foreach ($user->roles as $role) {
            echo $role->name . "<br>";
        }
        exit();
        //$admin = Role::where('name','=','admin')->first();
        //var_dump($admin);
        //$user->attachRole($admin); // Parameter can be an Role object, array or id.
        //$user->roles()->attach( $admin->id );

        if ($user->hasRole("admin"))
            echo "user has admin role<br>";
        else
            echo "user don't have admin role<br>";
        if ($user->can("modify_properties"))
            echo "user can modify_properties<br>";
        else
            echo "user cannot modify users<br>";
    });

});// end of authentication routes

// route for   cron job script.
Route::get('/cron', 'UrlsController@cronJob');

// Confide routes

Route::get('users/create', 'UsersController@create');
Route::post('users', 'UsersController@store');
Route::get('/', 'UsersController@login');
Route::get('/users/login', 'UsersController@login');
Route::post('users/login', 'UsersController@doLogin');
Route::get('users/confirm/{code}', 'UsersController@confirm');
Route::get('users/forgot_password', 'UsersController@forgotPassword');
Route::post('users/forgot_password', 'UsersController@doForgotPassword');
Route::get('users/reset_password/{token}', 'UsersController@resetPassword');
Route::post('users/reset_password', 'UsersController@doResetPassword');
Route::get('users/logout', 'UsersController@logout');

